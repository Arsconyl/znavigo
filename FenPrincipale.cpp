#include <QtWebKitWidgets/QWebView>
#include <QtWebEngineWidgets/QWebEngineView>
#include <iostream>
#include "FenPrincipale.h"

FenPrincipale::FenPrincipale()
{
    setActions();
    setMenus();
    setToolbar();
    setOnglets();
    connections();
    setStatusBar();
    setMinimumSize(800, 800);
//    showMaximized();
}

FenPrincipale::~FenPrincipale() = default;

void FenPrincipale::setActions()
{
    quitter = new QAction("&Quitter");
    precedent = new QAction("&Précédent");
    suivant = new QAction("&Suivant");
    actualiser = new QAction("&Actualiser");
    accueil = new QAction("&Accueil");
    lancer = new QAction("&Lancer");
    arreter = new QAction("&Arrêter");
    nOnglet = new QAction("Nouvel onglet");
    fOnglet = new QAction("Fermer l'onglet");
    apropos = new QAction("A propos de zNavigo");
    aproposqt = new QAction("A propos de Qt");

    apropos = new QAction("A propos de zNavigo");
    aproposqt = new QAction("A propos de Qt");

    quitter->setShortcut(QKeySequence("Ctrl+Q"));
    quitter->setIcon(QIcon(ICONES+"stop.png"));

    precedent->setIcon(QIcon(ICONES+"prec.png"));

    suivant->setIcon(QIcon(ICONES+"suiv.png"));

    actualiser->setIcon(QIcon(ICONES+"actu.png"));

    accueil->setIcon(QIcon(ICONES+"home.png"));

    lancer->setIcon(QIcon(ICONES+"go.png"));

    arreter->setIcon(QIcon(ICONES+"stop.png"));
    connect(quitter, SIGNAL(triggered()), qApp, SLOT(quit()));
}

void FenPrincipale::setMenus()
{
    fichier = menuBar()->addMenu("&Fichier");
    fichier->addAction(nOnglet);
    fichier->addAction(fOnglet);
    fichier->addAction(quitter);

    navigation = menuBar()->addMenu("&Navigation");
    navigation->addAction(precedent);
    navigation->addAction(suivant);
    navigation->addAction(actualiser);
    navigation->addAction(accueil);
    navigation->addAction(lancer);
    navigation->addAction(arreter);

    aide = menuBar()->addMenu("?");
    aide->addAction(apropos);
    aide->addAction(aproposqt);
}

void FenPrincipale::setToolbar()
{
    saisie = new QLineEdit("https://www.google.com/");

    barre = addToolBar("barre");

    barre->addAction(precedent);
    barre->addAction(suivant);
    barre->addAction(arreter);
    barre->addAction(actualiser);
    barre->addAction(accueil);

    barre->addWidget(saisie);

    barre->addAction(lancer);
}

void FenPrincipale::setOnglets()
{
    ajouterOnglet(new QWebEngineView);
    onglets = new QTabWidget(this);
    onglets->setMovable(true);
    onglets->setTabsClosable(true);
    onglets->setTabShape(QTabWidget::Rounded);

    onglets->addTab(pageactuelle, "Nouvel onglet");
    onglets->addTab(new QLabel("Add tabs by pressing \"+\""), "+");
    setCentralWidget(onglets);

    chargePage();
    pageactuelle->show();
}

void FenPrincipale::connections()
{
    connect(actualiser, SIGNAL(triggered()), this, SLOT(recharger()));
    connect(precedent, SIGNAL(triggered()), this, SLOT(prec()));
    connect(suivant, SIGNAL(triggered()), this, SLOT(suiv()));
    connect(lancer, SIGNAL(triggered()), this, SLOT(chargePage()));
    connect(arreter, SIGNAL(triggered()), this, SLOT(arreterChargement()));
    connect(accueil, SIGNAL(triggered()), this, SLOT(retourAccueil()));
    connect(saisie, SIGNAL(returnPressed()), this, SLOT(chargePage()));
    connect(pageactuelle, SIGNAL(loadProgress(int)), this, SLOT(pageChargee(int)));
    connect(onglets, SIGNAL(currentChanged(int)), this, SLOT(changeOnglet(int)));
    connect(onglets->tabBar(), SIGNAL(tabBarClicked(int)), this, SLOT(nouvelOnglet(int)));
    connect(onglets, SIGNAL(tabCloseRequested(int)),this, SLOT(fermerOnglet(int)));
    connect(nOnglet, SIGNAL(triggered()), this, SLOT(nouvelOnglet()));
    connect(fOnglet, SIGNAL(triggered()), this, SLOT(fermerOnglet()));
    connect(apropos, SIGNAL(triggered()), this, SLOT(dialogZ()));
    connect(aproposqt, SIGNAL(triggered()), this, SLOT(dialogQt()));
}


void FenPrincipale::setStatusBar()
{
    progression = new QProgressBar;
    statusBar()->addWidget(progression);
    connect(pageactuelle, SIGNAL(loadProgress(int)), progression, SLOT(setValue(int)));
}

void FenPrincipale::chargePage()
{
    pageactuelle->load(QUrl(saisie->text()));
    pageactuelle->show();
}

void FenPrincipale::prec()
{
    pageactuelle->back();
}

void FenPrincipale::suiv()
{
    pageactuelle->forward();
}

void FenPrincipale::recharger()
{
    pageactuelle->reload();
}

void FenPrincipale::arreterChargement()
{
    pageactuelle->stop();
}

void FenPrincipale::retourAccueil()
{
    saisie->setText("https://www.google.com/");
    chargePage();
}

void FenPrincipale::pageChargee(int progres)
{
    if(progres > 50)
    {
        saisie->setText(pageactuelle->url().toString());
        auto titre = pageactuelle->title();
        onglets->setTabToolTip(onglets->indexOf(pageactuelle), titre);
        if (titre.length() > 20)
        {
            titre.resize(20);
            titre += "...";
        }
        onglets->setTabText(onglets->indexOf(pageactuelle), titre);
    }
}

void FenPrincipale::nouvelOnglet()
{
        auto *o = new QWebEngineView;
        onglets->insertTab(onglets->count()-1, o, "");
        ajouterOnglet(o);
        saisie->setText("https://www.google.com/");
        o->load(QUrl(saisie->text()));
}

void FenPrincipale::nouvelOnglet(int index)
{

    if(index == onglets->count()-1)
    {
        auto *o = new QWebEngineView;
        onglets->insertTab(onglets->count()-1, o, "");
        ajouterOnglet(o);
        saisie->setText("https://www.google.com/");
        o->load(QUrl(saisie->text()));
    }
}

void FenPrincipale::changeOnglet(int index)
{
    pageactuelle = dynamic_cast<QWebEngineView *>(onglets->widget(index));
    saisie->setText(pageactuelle->url().toString());
}

void FenPrincipale::ajouterOnglet(QWebEngineView *page)
{
    pageactuelle = page;
    connect(pageactuelle, SIGNAL(loadProgress(int)), this, SLOT(pageChargee(int)));
}

void FenPrincipale::fermerOnglet(int index)
{
    if (onglets->count() == 2)
    {
        nouvelOnglet(1);
    }
    if (index != onglets->count()-1)
    {
        onglets->setCurrentIndex(index-1);
        onglets->removeTab(index);
    }
}

void FenPrincipale::fermerOnglet()
{
    int index = onglets->indexOf(pageactuelle);
    if (onglets->count() == 2)
    {
        nouvelOnglet(1);
    }
    if (index != onglets->count()-1)
    {
        onglets->setCurrentIndex(index-1);
        onglets->removeTab(index);
    }
}

void FenPrincipale::dialogZ()
{
    QMessageBox::about(this, "A propos de zNavigo", "zNavigo est un projet réalisé pour illustrer les tutoriels C++ du <a href=\\\"http://www.siteduzero.com\\\">Site du Zéro</a>.<br />Les images de ce programme ont été créées par <a href=\\\"http://www.everaldo.com\\\">Everaldo Coelho</a>");
}

void FenPrincipale::dialogQt()
{
    QMessageBox::aboutQt(this);
}


