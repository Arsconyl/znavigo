#include <QApplication>
#include <QtWidgets>
#include <QtWebEngine>
#include <QtWebEngineWidgets>
#include <QtWebKit>
#include "FenPrincipale.h"


int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    QtWebEngine::initialize();

    FenPrincipale fenetre;
    fenetre.show();

    return QApplication::exec();
}